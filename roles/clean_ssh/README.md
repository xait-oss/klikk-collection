clean_ssh
=========

Recreate ssh keys on hosts and add them to local known_hosts.

Remove the old keys if the host IPs are known, like provisioned cloud vms that are destroyed and recreated.

Only requires password free ssh-access to hosts.


Example Playbook
----------------


    - hosts: all
      roles:
         - role: xait.klikk.clean_ssh

License
-------

GPL-3.0-or-later
