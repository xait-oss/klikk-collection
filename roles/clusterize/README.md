clusterize
=========

Add inbound rules on klikk to all hosts to allow connections from all other hosts.

Workaround until easy firewall groups are available.

Requirements
------------

Uses the xait.klikk.firewall_rules module.

All hosts need the name of the vm on klikk.

Role expects all cluster nodes to be in the same inventory group.

Role Variables
--------------

You must include the JID_SSO `session_token` gotten from inspecting cookies when logged in at my.klikk.com

You must include a description for the cluster to identify the firewall rule. Old rules of the same name will not be updated.

You must specify the inventory group of the nodes with cluster_group_name.

You may specify the protocol for the rule, (blank, "any", "tcp", "udp", "icmp"), default is "any".

You may specify only ports to allow. Blank for all ports.
Specify as a list of string. Ranges are allowed, ("20:30")


Example Playbook
----------------


    - hosts: clusterize
      roles:
         - role: xait.klikk.clusterize
           vars:
             session_token: 'dfsaøøldføkasd'
             description: "my cluster"
             cluster_group_name: "clusterize"
             protocol: "tcp"
             ports:
             - 5050
             - 80
             - 443
             - 10000:10002

License
-------

GPL-3.0-or-later
