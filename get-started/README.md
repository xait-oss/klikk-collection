# Get started with Klikk

This guide takes you through setting up a VM on Klikk using this ansible collection.
Prerequisites:
- A Klikk.com account
- Ansible installed
- An ssh key
- Know your public IP


Steps:
1. Clone this repository
2. Move into the repository
3. Run `ansible-galaxy collection install .`
4. Move into this directory
5. Configure VMs in [`inventory.yml`](./inventory.yml)
6. Configure ssh-keys, IPs and OS in [`playbook.yml`](./playbook.yml)
7. [Export your credentials](#exporting-your-credentials)
8. Run `ansible-playbook playbook.yml -i inv.klikk.yml -i inventory.yml -u root`

## Exporting your credentials
```sh
export KLIKK_USER=""
export KLIKK_PASS=""
# If on a Mac, you can use the pbpast utility to export directly from your clipboard.
# Just copy the username or password and run the corresponding command.
export KLIKK_USER="$(pbpaste)"
export KLIKK_PASS="$(pbpaste)"
```



## Detailed explanation
First, clone this repository and move into it. Here, you have to run `ansible-galaxy collection install .` which will install this collection so ansible can use it.

Move into this directory. In this directory you will find three important files, [`inv.klikk.yml`](./inv.klikk.yml), [`inventory.yml`](./inventory.yml) and [`playbook.yml`](./playbook.yml). `inventory.yml` is where you define the VMs you want to create, including specifications like cpu cores, ram and storage. We don't specify an IP like we would in most ansible inventories since the target and IP do not yet exist. This is where `inv.klikk.yml` comes in. It is a plugin that fetches all the VMs in your account and merges that list with any other inventory you specify. This makes sure that if you specify both `inventory.yml` and `inv.klikk.yml` as inventories, you won't have to worry about creating duplicates with the same name, and if you later want to use antoher playbook to work with a VM directly you only need to specify the name, and the plugin will find the corresponding VM and IP from Klikk if it exists.

With our inventory and plugin configured, we can configure `playbook.yml`. This is where we actually create the VM. `playbook.yml` contains a lot of template variables like `{{ inventory_hostname }}` which it uses to take specifications from the inventory for the VMs it creates. These template variables should not be touched. To configure your VMs for use, you have to specify the list of allowed ssh keys and the ips that are allowed to connect with ssh. These are the `authorized_keys` and `ssh_IP_allow` fields. You can also optionally expose any port to the internet via tcp using the `public_ports_allow` field, check one of the [examples](../examples/) for more.

Finally export your credentials as show in [the section above](#exporting-your-credentials) and run the following command:
```sh
ansible-playbook playbook.yml -i inv.klikk.yml -i inventory.yml -u root
```
This will create your VMs on Klikk.com and you should be able to run `ansible-inventory -i inventory.yml -i inv.klikk.yml --graph --vars groupname` to see your new VMs with their new IPs.

## Firewall
You can optionally add your VMs to a firewall group. These groups have to be created by Klikk.com at your request. To add the VMs to a firewall group, you add the fields `firewall_addme: true` and `firewall_id: "<group id>"` to `options:` part of `playbook.yml`. Replace `<group id>` with the id of your firewall group.

> [!WARNING]
> Firewall groups might have problems with updating when using the API via this collection, meaning you might have to make some manual change to the group for all the VMs to use the configured firewall configurations.