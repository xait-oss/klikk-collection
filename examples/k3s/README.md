# K3S example
---

This example sets up a vps cluster on klikk, allows all traffic between the
nodes in the cluster and uses `https://github.com/k3s-io/k3s-ansible` to 
setup k3s on the cluster.


# Prerequisites
---

Clone `https://github.com/k3s-io/k3s-ansible` to you computer and `cd` into the directory.

Ensure this collection is installed.

Copy `./example.yml`, `./inv.klikk.yml` and `./inventory.yml` into the repo.
Insert credentials where they are needed, or comment out the lines.

Configure `inventory.yml` further as needed, finally running:
```sh
ansible-playbook example.yml -i inv.klikk.yml -i inventory.yml -u root
```

This should leave you with a working k3s cluster on klikk.

The automation automatically copies the kubeconfig to your computer at `~/.kube/config`
