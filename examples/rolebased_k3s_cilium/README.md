# K3S with Cilium example
---

This example sets up a vps cluster on klikk, allows all traffic between the
nodes in the cluster and uses the xanmanning.k3s / https://github.com/PyratLabs/ansible-role-k3s role to install k3s on the nodes.


We configure k3s to start without a CNI and install Cilium instead using a HELM chart


# Prerequisites
---

Install xanmanning.k3s:
```sh
ansible-galaxy install xanmanning.k3s
```

Ensure this collection is installed.

Insert credentials where they are needed, or comment out the lines.

Configure `inventory.yml` further as needed, finally running:
```sh
ansible-playbook example.yml -i inv.klikk.yml -i inventory.yml -u root
```

This should leave you with a working k3s cluster on klikk.

# Installing Cilium
---

Requires the ansible core kubernetes collection

Run the `add_cilium.yml` playbook with the same inventory as used to setup k3s.
This installs helm on the master nodes and installs Cilium using helm.
Modify the `cilium_values` field in `inventory.yml` to set values for cilium.


