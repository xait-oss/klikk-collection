# K3S with Cilium example
---

This example sets up a vps cluster on klikk, allows all traffic between the
nodes in the cluster and uses `https://github.com/k3s-io/k3s-ansible` to 
setup k3s on the cluster.

We configure k3s to start without a CNI and install Cilium instead using a HELM chart


# Prerequisites
---

Clone `https://github.com/k3s-io/k3s-ansible` to you computer and `cd` into the directory.

Ensure this collection is installed.

Copy `./example.yml`, `./inv.klikk.yml` and `./inventory.yml` into the repo.
Insert credentials where they are needed, or comment out the lines.

Configure `inventory.yml` further as needed, finally running:
```sh
ansible-playbook example.yml -i inv.klikk.yml -i inventory.yml -u root
```

This should leave you with a working k3s cluster on klikk.

The automation automatically copies the kubeconfig to your computer at `~/.kube/config`

# Installing Cilium
---

Requires the ansible core kubernetes collection

Run the `add_cilium.yml` playbook with the same inventory as used to setup k3s.
This installs helm on the master nodes and installs Cilium using helm.
Modify the `cilium_values` field in `inventory.yml` to set values for cilium.

