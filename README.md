# Ansible Collection - xait.klikk
---

Collection of roles and plugins to automate actions on cloud service provider Klikk.com

## IMPORTANT
---
> [!IMPORTANT]
> Some of the modules in this collection are using the internal website API as a workaround 
> while more features are added to the public API. This requires getting the `JID_SSO` session 
> token from the cookies when you log into `my.klikk.com`
> 
> This concerns the `firewall_rules` and `vm_power` modules as well as the `clusterize` role.

## Available content
---

Documentation for modules are currently only found in the module source code.

[Modules](/plugins/modules/README.md):
- firewall_rules
- vm_power
- create_vps

Inventory plugins:
- klikk

Roles:
- clean_ssh
- clusterize

## Credentials
---

Content in this collection use two sets of credentials.
The normal password and username can be set as environment variables:
```sh
export KLIKK_USER='klikk username'
export KLIKK_PASS='klikk password'
```

while the `JID_SSO` token must be put into the playbooks where it is needed for now.

The `JID_SSO` token can be found by going into developer developer tools while on your `my.klikk.com` 
dashboard. It should be available in the `Application` tab under `Storage/cookies`

## Examples
---
See [getting started](./get-started/README.md) for a quick introduction to using this collection for basic VM setup.

Examples of usecases are available in [examples/](./examples/)

## Installation
---

This collection can be installed locally by cloning this repo and running `ansible-galaxy collection install .` inside this directory.

