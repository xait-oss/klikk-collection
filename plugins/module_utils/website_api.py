from ansible.module_utils.urls import fetch_url
from ansible.module_utils.basic import AnsibleModule
from typing import Union
import json

""" Functions to interact with klikk's website API.
"""
WEBSITE_API_ENDPOINT = "https://my.klikk.com/"


def klikk_headers(
    session_token: str, method: str = "GET", data: Union[dict, None] = None
) -> dict:
    """Create headers for requests to klikk"""
    headers = {
        "Cookie": "JID_SSO={}".format(session_token),
        "Accept": "application/json",
    }
    if data is not None:
        headers["Content-Length"] = len(data)
    if method == "GET":
        headers["Content-Type"] = "application/json"
    elif method == "POST":
        headers["Content-Type"] = "application/x-www-form-urlencoded"
    return headers


def get_vm_id(module: AnsibleModule, vm_name: str, headers: dict) -> int:
    """Get id of vm from name

    Fails on not finding a vm by name
    """
    response, info = fetch_url(
        module=module,
        headers=headers,
        url=WEBSITE_API_ENDPOINT + "servers?p=servers",
        method="GET",
    )
    body = response.read()
    if info["status"] >= 400:
        body = info["body"]
        module.fail_json("Failed request for servers: " + body)
    if response is None:
        module.fail_json("Failed request for servers")
    try:
        body = json.loads(body)
        if body.get("status") == 500:
            module.fail_json("Failed request for servers: " + str(body))
        servers = body["result"]["servers"]
        if servers is None:
            module.fail_json("No servers found: " + json.dumps(body))
        for server in servers:
            if server["serverName"] == vm_name:
                return server["serverId"]
        # Fails because missing vm most likely means your
        # playbook can't do what you want it to do
        module.fail_json("Could not find server by name {}".format(vm_name))
    except json.JSONDecodeError as e:
        module.fail_json("Failed to load response(get_vm_id): " + str(e), body=body)
    except KeyError as e:
        module.fail_json("Failed to read response: " + str(e))


def get_vm(module: AnsibleModule, vm_id: int, headers: dict) -> dict:
    """Get id of vm from name

    Fails on not finding a vm by id
    """
    response, info = fetch_url(
        module=module,
        headers=headers,
        url=WEBSITE_API_ENDPOINT + "servers_edit?p=&serverId={}".format(vm_id),
        method="GET",
    )
    body = response.read()
    if info["status"] >= 400:
        body = info["body"]
        module.fail_json("Failed request for vm: " + body)
    if response is None:
        module.fail_json("Failed request for vm")
    try:
        body = json.loads(body)
        if body.get("status") == 500:
            module.fail_json("Failed request for vm: " + str(body))
        vm = body["result"].get("server")
        if vm is None:
            # Fails because missing vm most likely means your
            # playbook can't do what you want it to do
            module.fail_json("Could not find vm with id {}".format(vm_id))
        return vm
    except json.JSONDecodeError as e:
        module.fail_json("Failed to load response(get_vm): " + str(e), body=body)
    except KeyError as e:
        module.fail_json("Failed to read response: " + str(e))


def get_network(module: AnsibleModule, vm_id: int, headers: dict) -> list:
    """Get networks of the given vm

    Fails on not finding a network for the given vm id
    """
    response, info = fetch_url(
        module=module,
        headers=headers,
        url=WEBSITE_API_ENDPOINT + "servers_edit?p=network&serverId={}".format(vm_id),
        method="GET",
    )
    body = response.read()
    if info["status"] >= 400:
        body = info["body"]
        module.fail_json("Failed request for network: " + body)
    if response is None:
        module.fail_json("Failed request for network")
    try:
        body = json.loads(body)
        if body.get("status") == 500:
            module.fail_json("Failed request for network: " + str(body))
        networks = body["result"].get("network")
        if networks is None or len(networks) == 0:
            # Fails because missing vm most likely means your
            # playbook can't do what you want it to do
            module.fail_json("Could not find network for vm with id {}".format(vm_id))
        return networks
    except json.JSONDecodeError as e:
        module.fail_json("Failed to load response(get_network): " + str(e), body=body)
    except KeyError as e:
        module.fail_json("Failed to read response: " + str(e))


def get_firewall_rule(
    module: AnsibleModule, vm_id: int, rule_name: str, headers: dict
) -> Union[dict, None]:
    """Get firewall rule from name"""
    rules = get_firewall(module, vm_id, headers)
    for rule in rules:
        if rule["ruleDescription"] == rule_name:
            return rule
    else:
        return None


def get_firewall(module: AnsibleModule, vm_id: int, headers: dict) -> list:
    """Get list of firewall ruler for vm"""
    response, info = fetch_url(
        module=module,
        headers=headers,
        url=WEBSITE_API_ENDPOINT + "servers_edit?p=firewalls&serverId={}".format(vm_id),
        method="GET",
    )
    body = response.read()
    if info["status"] >= 400:
        body = info["body"]
        module.fail_json("Failed request for firewall rules: " + body)
    if response is None:
        module.fail_json("Failed request for firewall rules")
    try:
        body = json.loads(body)
        if body.get("status") == 500:
            module.fail_json("Failed to reqeust firewall rules: " + str(body))
        rules = body["result"]["firewallRules"]
        if rules is None:
            module.fail_json("No firewall rules found: " + json.dumps(body))
        return rules
    except json.JSONDecodeError as e:
        module.fail_json("Failed to load response(get_firewall): " + str(e), body=body)
    except KeyError as e:
        module.fail_json("Failed to read response: " + str(e))


def delete_firewall_rule(module: AnsibleModule, rule: dict, headers: dict) -> bool:
    """Delete a firewall rule from vm"""
    query = "a=delete&serverId={}&vm_network_id={}&firewall_rule_id={}".format(
        rule["serverId"], rule["networkId"], rule["ruleId"]  # Actual firewall
    )
    response, info = fetch_url(
        module=module,
        headers=headers,
        url=WEBSITE_API_ENDPOINT + "servers_edit?p=firewalls&{}".format(query),
        method="POST",
        data=query,
    )
    status_code = info["status"]
    body = response.read()
    if status_code >= 400:
        body = info["body"]
        module.fail_json("Failed to delete firewall rule: " + str(json.loads(body)))
    try:
        body = json.loads(body)
        if body.get("status") == 500:
            module.fail_json("Failed to delete rule: " + str(body))
        return True
    except json.JSONDecodeError as e:
        module.fail_json(
            "Failed to load response(delete_firewall_rule): " + str(e), body=body
        )


def create_firewall_rule(module: AnsibleModule, rule: dict, headers: dict) -> dict:
    """Create a firewall rule for vm"""
    query = "serverId={}&vm_network_id={}".format(rule["ServerId"], rule["NetworkId"])
    data = "action=firewall&vm_network_id={}&serverId={}&description={}&\
        destination_port={}&direction={}&source_ip={}&type={}".format(
        rule["NetworkId"],
        rule["ServerId"],
        rule["RuleDescription"],
        rule["RulePort"],
        rule["RuleType"],
        rule["RuleIp"],
        rule["RuleProtocol"],
    )
    response, info = fetch_url(
        module=module,
        headers=headers,
        url=WEBSITE_API_ENDPOINT + "servers_edit?p=firewalls&{}".format(query),
        method="POST",
        data=data,
    )
    status_code = info["status"]
    body = response.read()
    if status_code >= 400:
        body = info["body"]
        module.fail_json("Failed to create firewall rule: " + str(json.loads(body)))
    try:
        body = json.loads(body)
        if body.get("status") == 500:
            module.fail_json("Failed to create rule: " + str(body))
        rules = body["result"].get("firewallRules")
        if rules is None:
            module.fail_json("No firewall rules found: " + json.dumps(body))
        for _rule in rules:
            if _rule["ruleDescription"] == rule["RuleDescription"]:
                return _rule
        else:
            module.fail_json("Could not find created rule")
    except json.JSONDecodeError as e:
        module.fail_json(
            "Failed to load response(create_firewall_rule): " + str(e), body=body
        )
    except KeyError as e:
        module.fail_json("Failed to compare rules: " + str(e))


def set_vm_state(module: AnsibleModule, vm_id: int, state: int, headers: dict) -> dict:
    """Updated power state of vm"""
    if state == 0:
        action = "vm_power_off"
    else:
        action = "vm_power_on"
    query = "action={}serverId={}".format(action, vm_id)
    response, info = fetch_url(
        module=module,
        headers=headers,
        url=WEBSITE_API_ENDPOINT + "servers_edit?p=&serverId={}".format(vm_id),
        method="POST",
        data=query,
    )
    status_code = info["status"]
    body = response.read()
    if status_code >= 400:
        body = info["body"]
        module.fail_json("Failed to change power state1: " + str(json.loads(body)))
    try:
        body = json.loads(body)
        if body.get("status") == 500:
            if body["message"] == "-1 VM is busy/locked":
                return {
                    "State": 0 if state == 1 else 0
                }  # status 500 means busy vm, changing stat
            module.fail_json("Failed to change power state2: " + str(body))
        vm = body["result"].get("server")
        if vm is None:
            module.fail_json("Failed to change power state3: " + str(body))
        return vm
    except json.JSONDecodeError as e:
        module.fail_json("Failed to load response(set_vm_state): " + str(e), body=body)
