from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import fetch_url
from ansible_collections.xait.klikk.plugins.module_utils.errors import (
    KlikkConnectionError,
)
import json
import time
from typing import Union


""" Functions to interact with klikk's public API.
"""
PUBLIC_API_ENDPOINT = "https://my.klikk.com/api"

HEADERS = {"Accept": "application/json", "Content-Type": "application/json"}


def run_query(module: AnsibleModule, data: dict) -> dict:
    """Run query against API and return valid JSON response"""
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
    }

    response, info = fetch_url(
        module=module,
        url=PUBLIC_API_ENDPOINT,
        headers=headers,
        method="POST",
        data=module.jsonify(data),
    )
    if response is None:
        if "body" in info:
            tmp = json.loads(info["body"])
            if "items" in tmp:
                return tmp["items"][0]
            return tmp
        raise KlikkConnectionError("Request failed")

    try:
        return json.load(response)
    except ValueError:
        raise KlikkConnectionError(f"Incorrect JSON payload: {response}, {info}")


def get_vms(module: AnsibleModule) -> Union[list, None]:
    """Return list of all available vms.

    :returns: List of vms or None if no vms are found.
    """
    data = {"action": "server_list"}
    response = run_query(module, data)
    servers = response["servers"]
    if len(servers) == 0:
        return None
    return servers


def get_vm_by_name(module: AnsibleModule, vm_name: str) -> Union[dict, None]:
    """Return vm with the given name.

    :returns: Dictionary of vm or None if no vm is found.
    """
    data = {"action": "server_list"}
    response = run_query(module, data)
    servers = response["servers"]
    for server in servers:
        if server["name"] == vm_name:
            return server
    return None


def get_vm_by_id(module: AnsibleModule, vm_id: int) -> Union[dict, None]:
    """Return vm with given id.

    :returns: Dictionary of vm or None if no vm is found.
    """
    data = {"action": "server_list"}
    response = run_query(module, data)
    servers = response["servers"]
    for server in servers:
        if server["id"] == vm_id:
            return server
    return None


def get_vm_id(module: AnsibleModule, vm_name: str) -> Union[int, None]:
    """Return id of the vm with the given name.

    :returns: Id of vm or None if no vm is found.
    """
    vm = get_vm_by_name(module, vm_name)
    if vm is None:
        return None
    return vm["id"]


def get_order_by_vm_name(module: AnsibleModule, vm_name: str) -> Union[dict, None]:
    """Return order object containing order item corresponding to vm_name

    :returns: Dictionary of order or None if no vm/order is found.
    """
    vm = get_vm_by_name(module, vm_name)
    if vm is None:
        return None
    data = {"action": "order_list"}
    response = run_query(module, data)
    orders = response["orders"]
    for order in orders:
        for order_item in order["order_items"]:
            if order_item["id"] == vm["order_item_id"]:
                return order
    return None


def get_order_by_order_item_id(
    module: AnsibleModule, order_item_id: int
) -> Union[dict, None]:
    """Return order object containing order item corresponding to given order_item_id.

    :returns: Dictionary of order or None if no matching order_item_id.
    """
    data = {"action": "order_list"}
    response = run_query(module, data)
    orders = response["orders"]
    for order in orders:
        for order_item in order["order_items"]:
            if order_item["id"] == order_item_id:
                return order
    return None


def get_vm_by_order_item_id(
    module: AnsibleModule, order_item_id: int
) -> Union[dict, None]:
    """Return vm with given order_item_id.

    :returns: Dictionary of vm or None if no vm found.
    """
    data = {"action": "server_list", "order_item_id": order_item_id}
    response = run_query(module, data)
    servers = response["servers"]
    if len(servers) == 0:
        return None
    return servers[0]


def delete_vm_by_name(module: AnsibleModule, vm_name: str) -> Union[dict, None]:
    # TODO Need to rewrite the errorhandling for requests.
    # Deleting something returns a status 200,
    # but the response is not None but neither a valid json object
    # if module.params['state'] == 'absent':
    #     result['changed'] = vps_delete(module, result['name'])
    #     if result['changed']:
    #         result['message'] = f'Vps {result["name"]} deleted.'
    #     else:
    #         result['message'] = 'Nothing changed'
    #         module.exit_json(**result)
    # TODO: Test responses for pending deletion, etc.
    """Cancel vm with given name.

    :returns: Dictionary of deleted vm or None if no vm found to delete.
    """
    vm = get_vm_by_name(module, vm_name)
    if vm is None:
        return None
    order = get_order_by_vm_name(module, vm_name)
    if order is None:
        return False  # No vm to delete
    for item in order["order_items"]:
        if item["id"] == vm["order_item_id"]:
            order_item_id = item["id"]
            break
    else:
        raise KlikkConnectionError("Found no order with vm by name: {}".format(vm_name))
    data = {
        "action": "order_cancel",
        "order_id": order["id"],
        "order_item_id": order_item_id,
    }
    response = run_query(module, data)
    return response


def wait_order_complete(
    module: AnsibleModule,
    order_id: int,
    timeout: int = 60,
    interval: int = 1,
    fail_timeout: bool = False,
) -> Union[dict, None]:
    """Wait for order to be complete before returning.

    :arg timeout: Max number of seconds to wait
    :arg interval: Time to sleep between update requests
    :arg fail_timeout: Whether to raise KlikkConnectionError on timeout.
        False by default.

    :raises: KlikkConnectionError on timeout if fail_timeout is True
    :returns: Completed order or None on timeout
    """
    data = {
        "action": "order_status",
        "order_id": order_id,
    }
    wait_start = time.time()
    while timeout > (time.time() - wait_start):
        response = run_query(module, data)
        if response["status"] == 0:  # Order complete
            return response
        if not 200 <= response["status"] < 300:  # Pending creation: 2xx.
            raise KlikkConnectionError("Bad status code: {}".format(response["status"]))
        time.sleep(interval)
    if fail_timeout:
        raise KlikkConnectionError("Timed out")
    return None


def create_vm(module: AnsibleModule, *vm_specs: dict) -> dict:
    """Create vms with given vm specs"""
    data = {"action": "order_create", "items": [*vm_specs]}
    response = run_query(module, data)
    return response
