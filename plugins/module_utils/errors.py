class KlikkConnectionError(Exception):
    def __init__(self, msg: str = ""):
        super().__init__(msg)
        self._msg = msg

    @property
    def msg(self):
        return self._msg
