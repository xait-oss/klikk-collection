#!/usr/bin/python
# Template for modules

# Copyright: (c) 2022, Birk Lewin <birk.lewin@xait.com>
# GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function
from ansible.module_utils.basic import AnsibleModule, env_fallback
from ansible_collections.xait.klikk.plugins.module_utils.website_api import (
    klikk_headers,
    get_vm,
    get_vm_id,
    set_vm_state,
)
import time


__metaclass__ = type

DOCUMENTATION = r"""
---
module: vm_power
author:
    - Birk Lewin <birk.lewin@xait.com>

short_description: Start, stop or restart vm.
version_added: "1.1.0"

options:
    vm_name:
        description: VM selector to modify rules of
        required: true
        type: str
    session_token:
        description: Session token from logging in on my.klikk.com
        type: str
        required: true
    state:
        description: Action to perform
        type: str
        default: "on"
        choices: ["on", "off", "restarted"]
    confirm:
        description: Waiting for confirmed state change before continuing.
            Only affects "on" and "off"
        type: bool
        default: true
    wait_delay:
        description: How many seconds to wait between checking state.
        type: int
        default: 10
"""

EXAMPLES = r"""
xait.klikk.vm_state:
      vm_name: kubb_m
      session_token: 'dsafgasdcjgasguiewarrq453254236fewq'
      state: restarted
"""

RETURN = r"""
changed_vm:
    description: Dictionary of vm that was changed
    type: dict
    returned: on change
"""


def run_module():
    module_args = dict(
        session_token=dict(
            type="str",
            required=True,
            no_log=True,
            fallback=(env_fallback, ["KLIKK_JID_SSO"]),
        ),
        vm_name=dict(type="str", required=True),
        state=dict(type="str", choices=["on", "off", "restarted"], default="on"),
        confirm=dict(type="bool", default=True),
        wait_delay=dict(type="int", default=7),
    )
    result = dict(
        changed=False, original_message="", message="", vm_id="", changed_vm=None
    )
    module = AnsibleModule(argument_spec=module_args)
    vm_id = get_vm_id(
        module,
        module.params["vm_name"],
        klikk_headers(module.params["session_token"], "GET", None),
    )
    result["vm_id"] = vm_id
    vm = get_vm(
        module, vm_id, klikk_headers(module.params["session_token"], "GET", None)
    )
    sleep_time = module.params["wait_delay"] if module.params["wait_delay"] > 1 else 1

    if module.params["state"] == "on" and vm["State"] == 1:
        module.exit_json(**result)
    if module.params["state"] == "off" and vm["State"] == 0:
        module.exit_json(**result)
    if module.params["state"] == "off" or module.params["state"] == "restarted":
        vm = set_vm_state(
            module,
            vm_id,
            0,
            klikk_headers(module.params["session_token"], "POST", None),
        )
        if module.params["state"] == "restarted" or module.params["confirm"]:
            while True:
                time.sleep(sleep_time)
                vm = get_vm(
                    module,
                    vm_id,
                    klikk_headers(module.params["session_token"], "GET", None),
                )
                if vm["State"] == 0:
                    break
        result["changed"] = True

    if module.params["state"] == "on" or module.params["state"] == "restarted":
        vm = set_vm_state(
            module,
            vm_id,
            1,
            klikk_headers(module.params["session_token"], "POST", None),
        )
        if module.params["state"] == "restarted" or module.params["confirm"]:
            while True:
                time.sleep(sleep_time)
                vm = get_vm(
                    module,
                    vm_id,
                    klikk_headers(module.params["session_token"], "GET", None),
                )
                if vm["State"] == 1:
                    break
        result["changed"] = True
    result["changed_vm"] = vm

    module.exit_json(**result)


def main():
    run_module()


if __name__ == "__main__":
    main()
