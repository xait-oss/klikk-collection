#!/usr/bin/python
# Template for modules

# Copyright: (c) 2022, Birk Lewin <birk.lewin@xait.com>
# GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function
from ansible.module_utils.basic import AnsibleModule


__metaclass__ = type

DOCUMENTATION = r"""
"""

EXAMPLES = r"""
"""

RETURN = r"""
"""


def run_module():
    module_args = dict(
        session_token=dict(type="str", required=True),
        vm_name=dict(type="str", required=True),
        state=dict(type="str", choices=["present", "absent"], default="present"),
    )
    result = dict(changed=False, original_message="", message="")
    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == "__main__":
    main()
