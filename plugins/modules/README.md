# Modules
---


## xait.klikk.create_vps
---

Create a VPS on my.klikk.com using Ansible

Available options:
|**Option**|**Description**|
|---|---|
|delay|How long to wait before starting to create VPS. Useful for creating multiple VPS' in parallel|
|wait_order_complete|Blocks the playbook from continuing until the VPS order has been created|
|name|Name of the VPS|
|options.ram_gb|Gigabytes of memory in 0.5GB increments|
|options.storage_gb|Gigabytes of storage in 25GB increments|
|options.cpu|Number of virtual cpu cores|
|options.os|Operating system to use on the VPS|
|options.ipv4|True if you want an IPv4 address assigned, costs extra|
|options.authorized_keys|List of ssh public-keys to use as authorized keys on the created VPS|
|options.ssh_ip_allow|List of IP addresses to allow full access to. All ports open for these addresses|
|options.public_ports_allow|List of ports to allow for all IP addresses. Should never be port 22 as that would open ssh for anyone to access|
|options.firewall_id|Id of firewall to add VPS to. Supports only a single id|
|options.firewall_addme|True if VPS' IPv4 and IPv6 addresses should be added to the ingress rules for firewall options.firewall_id. This would allow all VPSs in the same firewall to freely communicate with each other.|