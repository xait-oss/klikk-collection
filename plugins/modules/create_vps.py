#!/usr/bin/python3

# Copyright: (c) 2021, Andrei Costescu <andrei@costescu.no>
# GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
from ansible.module_utils.basic import AnsibleModule, env_fallback
from ansible_collections.xait.klikk.plugins.module_utils import public_api as papi
from ansible_collections.xait.klikk.plugins.module_utils.errors import (
    KlikkConnectionError,
)
import time

__metaclass__ = type

DOCUMENTATION = r"""
---
module: create_vps

author:
  - Andrei Costescu (@cosandr)

short_description: Create new VPS on Klikk

description: >
  This module can be used to create a new VPS on the Klikk service,
  the API is limited at the moment and so are the options available
  for this module.

options:
  username:
    description: Klikk login username
    required: true
    type: str
  password:
    description: Klikk login password
    required: true
    type: str
  delay:
    description: Sleeptime before executing api call.
        Stagger api calls when executing many in a row to avoid overloading.
    type: int
  state:
    description: Create vps. Destroying is not yet supported.
    type: str
    choices: ["present", "absent"]
  name:
    description: VPS name
    required: true
    type: str
  wait_order_complete:
    description: Return once order is completed
    type: bool
    default: false
  options:
    type: dict
    description: Customize VPS
    suboptions:
      ram_gb:
        description:
          - RAM in GB
          - Must be between 0.5 GB and 500 GB
        type: float
      cpu:
        description:
          - Number of CPUs
          - Must be between 1 and 120
        type: int
      storage_gb:
        description:
          - Disk size in GB
          - Must be between 25 GB and 8000 GB
        type: int
      os:
        description: OS image
        type: str
        required: true
        choices: [ "CentOS", "Debian", "Flatcar", "JLinux", "Ubuntu", "Windows" ]
      os_version:
        description:
          - OS version
          - for example "10.4" for exact version, "10." for major
        type: str
      firewall_id:
        description: Id of firewall to make VPS part of. Usually used in conjunction with firewall_addme: true
        type: str
      firewall_addme:
        description: Set to true to add VPS' IPv4 and IPv6 to the firewalls ingress rules. Only effective in conjunction with firewall_id
        type: bool
      ipv4:
        description: Add public IPv4 address
        type: bool
      authorized_keys:
        description: SSH authorized keys
        type: list
        elements: str
      ssh_ip_allow:
        description:
          - List of IP addresses allowed to SSH
          - Required if authorized_keys is specified
        type: list
        elements: str
      public_ports_allow:
        description:
          - List of open ports
          - Cannot include port 22
        type: list
        elements: str

"""

EXAMPLES = r"""
---
xait.klikk.create_vps:
  username: example  # or export KLIKK_USER
  password: example  # or export KLIKK_PASS
  name: example01
  options:
    ram_gb: 1
    cpu: 2
    storage_gb: 50
    os: "Debian"
    ipv4: true
    authorized_keys:
      - "ssh-ed25519 ..."
      - "ssh-rsa ..."
    ssh_ip_allow:
      - "192.168.1.0/24"
      - "2a01::/64"
    public_ports_allow:
      - 80
      - 443
"""

RETURN = r"""
---
order_id:
  description: Created order ID.
  type: int
  returned: success
  sample: 73
status:
  description: Klikk status code.
  type: int
  returned: always
  sample: 200
message:
  description: Klikk message.
  type: str
  returned: always
  sample: 'Order is being processed'
name:
  description: VPS name
  type: str
  returned: always
  sample: 'My testserver 1'
product_spec:
  description: Requested product spec.
  type: str
  returned: valid arguments
"""


def run_module():
    """ """
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        delay=dict(type="int", default=0),
        state=dict(type="str", choices=["present", "absent"], default="present"),
        url_username=dict(
            type="str",
            required=True,
            aliases=["username", "user"],
            fallback=(env_fallback, ["KLIKK_USER"]),
        ),
        url_password=dict(
            type="str",
            required=True,
            aliases=["password", "pass"],
            fallback=(env_fallback, ["KLIKK_PASS"]),
            no_log=True,
        ),
        name=dict(type="str", required=True, aliases=["vps_name"]),
        wait=dict(type="bool", aliases=["wait_order_complete"], default=False),
        options=dict(
            type="dict",
            required=True,
            options=dict(
                ram_gb=dict(type="float", default=0.5),
                cpu=dict(type="int", default=1),
                storage_gb=dict(type="int", default=25),
                os=dict(type="str", required=True),
                os_version=dict(type="str"),
                ipv4=dict(type="bool", default=False),
                authorized_keys=dict(type="list", elements="str", default=[]),
                ssh_ip_allow=dict(type="list", elements="str", default=[]),
                public_ports_allow=dict(type="list", elements="str", default=[]),
                firewall_id=dict(type="str"),
                firewall_addme=dict(type="bool", default=False),
            ),
        ),
    )

    result = dict(
        changed=False,
        order_id=None,
        status=None,
        message="",
        name="",
        product_spec=None,
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True,
    )

    result["name"] = module.params["name"]
    options = module.params["options"]

    # Exit if vm already exists when creating
    # Exit if vm does not exist when removing
    try:
        vm = papi.get_vm_by_name(module, module.params["name"])
        if vm is not None and module.params["state"] == "present":
            result["message"] = "vps {} already exists".format(module.params["name"])
            result["product_spec"] = str(vm)
            module.exit_json(**result)
        elif vm is None and module.params["state"] == "absent":
            result["message"] = "no vps {} to remove".format(module.params["name"])
            module.exit_json(**result)
    except KeyError as e:
        module.fail_json(
            msg="[DEBUG]: Failed to access a dict", exception=str(e), **result
        )
    except KlikkConnectionError as e:
        module.fail_json(msg="API call failed", exception=e.msg, **result)

    # Validate options, collect all errors
    options_validation = []
    if 1 > options["cpu"] > 120 or options["cpu"] % 1 != 0:
        options_validation.append("CPUs must be between 1 and 120 in steps of 1")
    if 0.5 > options["ram_gb"] > 500 or check_point5(options["ram_gb"]):
        options_validation.append("RAM must be between 0.5 and 500 GB in steps of 0.5")
    if 25 > options["storage_gb"] > 8000 or options["storage_gb"] % 25 != 0:
        options_validation.append(
            "Storage must be between 25 and 8000 GB in steps of 25"
        )
    if options.get("authorized_keys") and not options.get("ssh_ip_allow"):
        options_validation.append("ssh_ip_allow is required when using authorized_keys")
    # Fail with all invalid option errors
    if len(options_validation) > 0:
        module.fail_json("\n".join(options_validation), **result)

    # Initialize and populate product spec
    product_spec = {
        "quantity_ram_gb": options["ram_gb"],
        "quantity_cpu": options["cpu"],
        "quantity_storage_gb": options["storage_gb"],
        "os": options["os"],
        "features": {},
    }
    os_version = options.get("os_version")
    if os_version:
        product_spec["os_version"] = os_version
    if options.get("ipv4"):
        product_spec["features"]["ipv4"] = 1
    if options.get("firewall_id"):
        product_spec["firewall_id"] = options.get("firewall_id")
    if options.get("firewall_addme"):
        product_spec["firewall_addme"] = 1
    if any(
        (
            options.get("authorized_keys"),
            options.get("ssh_ip_allow"),
            options.get("public_ports_allow"),
        )
    ):
        product_spec["features"]["sshd"] = {}

    authorized_keys = "\n".join(options.get("authorized_keys", []))
    if authorized_keys:
        product_spec["features"]["sshd"]["authorized_keys"] = authorized_keys
    ip_allow = ",".join(options.get("ssh_ip_allow", []))
    if ip_allow:
        product_spec["features"]["sshd"]["ip_allow"] = ip_allow
    public_ports_allow = ",".join(options.get("public_ports_allow", []))
    if public_ports_allow:
        product_spec["features"]["sshd"]["public_ports_allow"] = public_ports_allow

    # Construct data payload
    result["product_spec"] = product_spec
    vm_spec = {
        "name": module.params["name"],
        "product": {
            "name": "VPS",
            "options": product_spec,
        },
    }

    # Exit on checkmode here. Result will contain specs of expected created vm.
    if module.check_mode:
        result["message"] += "Exiting with check_mode"
        module.exit_json(**result)

    # Delay allows for staggering of calls from playbook.
    # Create vm
    time.sleep(module.params["delay"])
    try:
        api_data = papi.create_vm(module, vm_spec)
    except KlikkConnectionError as e:
        module.fail_json(msg="API call failed", exception=e.msg, **result)

    # Gather status of new vm order
    result["order_id"] = api_data.get("order_id")
    result["status"] = api_data.get("status")
    result["message"] = api_data.get("message") or api_data.get("error_message")

    if not 200 <= result["status"] < 300:
        module.fail_json(msg="Order failed", **result)

    result["changed"] = True

    # Ping status of order while waiting until timeout or order is complete
    if module.params["wait"]:
        try:
            api_data = papi.wait_order_complete(module, result["order_id"], 1200)
            result["order_id"] = api_data.get("order_id")
            result["status"] = api_data.get("status")
            result["message"] = api_data.get("message")
        except KlikkConnectionError as e:
            module.fail_json(msg="Wait failed", exception=e.msg, **result)
    result["message"] = "Completed order"
    module.exit_json(**result)

def check_point5(float_val):
    string_val = str(float_val)
    whole, fraction = string_val.split(".")
    return fraction != "5"

def main():
    run_module()


if __name__ == "__main__":
    main()
