#!/usr/bin/python

# Copyright: (c) 2022, Birk Lewin <birk.lewin@xait.com>
# GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function
from ansible.module_utils.basic import AnsibleModule, env_fallback
from ansible_collections.xait.klikk.plugins.module_utils.website_api import (
    klikk_headers,
    get_vm_id,
    get_firewall,
    delete_firewall_rule,
    create_firewall_rule,
    get_network,
)

__metaclass__ = type

DOCUMENTATION = r"""
---
module: firewall_rules
author:
    - Birk Lewin <birk.lewin@xait.com>

short_description: Create and remove firewall rules from klikk vms
version_added: "1.0.0"

description:

options:
    state:
        description: Create or remove firewall rules
        type: str
        default: present
        choices: ["present", "absent"]
    vm_name:
        description: VM selector to modify rules of
        required: true
        type: str
    description:
        description: Describes rule. Used to select firewall rule when removing
        required: true
        type: str
    direction:
        description: Inbound or outbound rule
        type: str
        default: inbound
        choices: ["inbound", "outbound]
    protocol:
        description: Protocol for rule
        type: str
        default: any
        choices: ["any", "tcp", "udp", "icmp"]
    session_token:
        description: Session token from logging in on my.klikk.com
        type: str
        required: true
    ips:
        description: List of ips for rule to work on.
        type: list
        default: []
    ports:
        description: List of ports for rule to work on
        type: list
        default: []
"""

EXAMPLES = r"""
xait.klikk.firewall_rules:
    state: present
    vm_name: my_vm
    description: unique rule name on vm
    direction: inbound
    protocol: tcp
    session_token: sfascibeja89asdchabke32k
    ips:
    - 192.168.10.0/32
    ports:
    - 80
    - 443
    - 5000:5005
"""

RETURN = r"""
---
vm_id:
    description: Id of vm that was changed
    type: int
    returned: success
    sample: 6634
firewall_rule:
    description: Rule that was created or removed
    type: dict
    returned: success
    sample:{
            "AccountId": 31241,
            "NetworkId": 5017,
            "RuleDescription": "unique rule name on vm",
            "RuleId": 9363,
            "RuleIp": "129.242.79.122/32",
            "RulePort": "80,443,5000:5005",
            "RuleProtocol": "IPv4",
            "RuleType": 1,
            "ServerId": 6634
        }
"""


def run_module():
    module_args = dict(
        session_token=dict(
            type="str",
            required=True,
            no_log=True,
            fallback=(env_fallback, ["KLIKK_JID_SSO"]),
        ),
        vm_name=dict(type="str", required=True),
        description=dict(type="str", required=True),
        protocol=dict(type="str", default="any", choices=["tcp", "udp", "icmp", "any"]),
        direction=dict(type="str", default="inbound", choices=["inbound", "outbound"]),
        ips=dict(type="list", default=[]),
        ports=dict(type="list", default=[]),
        state=dict(type="str", choices=["present", "absent"], default="present"),
    )
    result = dict(
        changed=False,
        original_message="",
        message="",
        firewall_rule=None,
        vm_id=None,
    )
    module = AnsibleModule(
        argument_spec=module_args,
    )
    if (
        module.params.get("ips") is None
        and module.params.get("ports") is None
        and module.params["state"] == "present"
    ):
        module.fail_json(
            "Missing ports and ips. Remove all rules to allow all traffic.", **result
        )

    vm_id = get_vm_id(
        module,
        module.params["vm_name"],
        klikk_headers(module.params["session_token"], "GET", None),
    )
    result["vm_id"] = vm_id

    rules = get_firewall(
        module, vm_id, klikk_headers(module.params["session_token"], "GET", None)
    )
    exists = False
    firewall_rule = None
    for rule in rules:
        if rule["ruleDescription"] == module.params["description"]:
            firewall_rule = rule
            exists = True
            break

    if module.params["state"] == "absent":

        if not exists:
            result["message"] = "No firewall found: {}".format(
                module.params["description"]
            )
            module.exit_json(**result)
        delete_firewall_rule(
            module,
            firewall_rule,
            klikk_headers(module.params["session_token"], "POST", None),
        )
        result["changed"] = True
        result["message"] = "Removed firewall with description: {}".format(
            module.params["description"]
        )
        result["firewall_rule"] = firewall_rule

    elif module.params["state"] == "present":
        if exists:
            result["message"] = "Firewall already exists: {}".format(
                module.params["description"]
            )
            module.exit_json(**result)

        networks = get_network(
            module, vm_id, klikk_headers(module.params["session_token"], "GET", None)
        )
        network = networks[0]
        rule = {
            "ServerId": vm_id,
            "NetworkId": network["networkId"],
            "RuleType": module.params["direction"],
            "RuleDescription": module.params["description"],
            "RuleProtocol": module.params["protocol"],
            "RuleIp": ",".join((str(ip) for ip in module.params["ips"])),
            "RulePort": ",".join((str(port) for port in module.params["ports"])),
        }
        result["firewall_rule"] = create_firewall_rule(
            module, rule, klikk_headers(module.params["session_token"], "POST", None)
        )
        result["changed"] = True

    module.exit_json(**result)


def main():
    run_module()


if __name__ == "__main__":
    main()
