#!/usr/bin/python3

# Copyright: (c) 2021, Andrei Costescu <andrei@costescu.no>
# GNU General Public License v3.0+ (https://www.gnu.org/licenses/gpl-3.0.txt)
from ansible.plugins.inventory import BaseInventoryPlugin
from ansible.errors import AnsibleError
from ansible.module_utils.urls import open_url, to_text
from ansible_collections.xait.klikk.plugins.module_utils import public_api as papi
import json
__metaclass__ = type

DOCUMENTATION = r"""
---
name: klikk

author:
    - Andrei Costescu (@cosandr)

short_description: Klikk (klikk.com) inventory_plugins source

description: Fetch list of servers from Klikk.

options:
    plugin:
        description: token that ensures this is a source file for the 'klikk' plugin.
        required: True
        choices: ['xait.klikk.klikk']
    username:
        description: Klikk login username
        required: true
        type: str
        env:
            - name: KLIKK_USER
    password:
        description: Klikk login password
        required: true
        type: str
        env:
            - name: KLIKK_PASS
    ipv4_only:
        description: Skip VMs that only have IPv6 addresses
        default: false
        type: bool
"""

EXAMPLES = r"""
# inv.klikk.yml file in YAML format
# Example command line: ansible-inventory --list -i inv.klikk.yml
plugin: xait.klikk.klikk
username: "example"  # or export KLIKK_USER
password: "example"  # or export KLIKK_PASS
ipv4_only: true
"""


class InventoryModule(BaseInventoryPlugin):
    NAME = "klikk"

    def __init__(self):
        self._klikk_user = ""
        self._klikk_pass = ""
        self._klikk_data = {
            "action": "server_list"
        }
        self._klikk_data = json.dumps(self._klikk_data)
        super().__init__()

    def _fetch_info(self):
        response = open_url(
            url=papi.PUBLIC_API_ENDPOINT,
            headers=papi.HEADERS,
            method="POST",
            data=self._klikk_data,
            url_username=self._klikk_user,
            url_password=self._klikk_pass,
        )

        # https://github.com/ansible-collections/community.general/blob/main/plugins/inventory/online.py
        try:
            raw_data = to_text(response.read(), errors="surrogate_or_strict")
        except UnicodeError:
            raise AnsibleError(
                "Incorrect encoding of fetched payload from Klikk servers"
            )

        try:
            return json.loads(raw_data)
        except ValueError:
            raise AnsibleError("Incorrect JSON payload")

    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path)
        self._read_config_data(path=path)
        self._klikk_user = self.get_option("username")
        self._klikk_pass = self.get_option("password")
        ipv4_only = self.get_option("ipv4_only")

        api_data = self._fetch_info()
        for s in api_data.get("servers", []):
            if not s.get("networks") or not s.get("name"):
                continue
            net = s["networks"][0]
            if ipv4_only and not net.get("ipv4"):
                continue
            hostname = s["name"]
            self.inventory.add_host(host=hostname)
            if not ipv4_only:
                self.inventory.set_variable(hostname, "public_ipv6", net["ipv6"])
            if not net.get("ipv4"):
                self.inventory.set_variable(hostname, "ansible_host", net["ipv6"])
            else:
                self.inventory.set_variable(hostname, "public_ipv4", net["ipv4"])
                self.inventory.set_variable(hostname, "ansible_host", net["ipv4"])
            self.inventory.set_variable(hostname, "id", s["id"])
            self.inventory.set_variable(hostname, "running", bool(s["is_running"]))
            self.inventory.set_variable(hostname, "order_id", s["order_item_id"])
